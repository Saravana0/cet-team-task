import React from 'react';
import { BiChevronDown } from "react-icons/bi";
import  profile  from '../../Assets/Profile.png';
import { IoNotifications } from "react-icons/io5";

function header() {
  return (
    <div className="header-wrapper">
      <p> logo </p>
      <div className='right-header'>
        <IoNotifications size={25} className="IoNotifications"/>
        <div className='profile'>
          <span className='round'>
            <img src={profile}/>
          </span>
          <p className='profile_name gray-color'> Rocky </p>
          <BiChevronDown size={25} className="gray-color"/>
        </div>
        
      </div>
    </div>
  )
}

export default header