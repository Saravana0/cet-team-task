import React from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Button,
} from "reactstrap";
import { VscHome } from "react-icons/vsc";
import { TbUsers } from "react-icons/tb";
import { FaRegNewspaper } from "react-icons/fa";
import { BsChatLeftDots } from "react-icons/bs";
import { NavLink, Link } from "react-router-dom";


function Sidebar() {
  return (
    <div>
      <nav class="nav flex-column class-navBar">
        <NavLink activeClassName="active" to="/" class="nav-link active a-tag-padding" aria-current="page" href="#">
        <VscHome size={25} className="icon-color"/> 
          <p className="nav_bar_txt"> Dashboard </p>
        </NavLink>
        <a class="nav-link a-tag-padding" href="#">
          <TbUsers size={25} className="icon-color"/>
          <p className="nav_bar_txt"> Users </p>
        </a>
        <NavLink activeClassName="active" to="/news" class="nav-link a-tag-padding" href="#">
          <FaRegNewspaper size={25} className="icon-color"/>
          <p className="nav_bar_txt"> News </p>
        </NavLink>
        <a
          class="nav-link a-tag-padding"
          href="#"
          tabindex="-1"
          aria-disabled="true"
        >
          <BsChatLeftDots size={22} className="icon-color"/>
          <p className="nav_bar_txt"> Chat </p>
        </a>
      </nav>
    </div>
  );
}

export default Sidebar;
