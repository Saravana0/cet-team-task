import React from "react";
import { Button, Modal, ModalFooter, ModalHeader, ModalBody } from "reactstrap";

function Popup(props) {
  return (
    // <Modal isOpen={props.modal}
    //             toggle={props.toggle}
    //             className="popUp">
    //             <ModalBody>
    //                 Simple Modal with just ModalBody...
    //             </ModalBody>
    // </Modal>
    <Modal isOpen={props.modal} toggle={props.toggle}  size="lg"
    aria-labelledby="contained-modal-title-vcenter"
    centered>
      <ModalHeader toggle={props.toggle} className="d-flex align-items-start"> 
      <h3 className="mb-2"> Create a Post </h3>
      <p className="model-sub-header mt-0"> We appreciate your experience </p> 
      </ModalHeader>
      <ModalBody>{props.bodyContent}</ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={props.toggle} >
        Discord
        </Button>{" "}
        <Button color="secondary" onClick={props.submit}>
        Submit
        </Button>
      </ModalFooter>
    </Modal>
  );
}

export default Popup;
