import logo from './logo.svg';
import './App.css';
import Chat from './Pages/Chat/Chat'
import './Scss/cssFile.scss'
// import 'bootstrap/dist/css/bootstrap.min.css';
// import PrivateRoute from "./mainLayout/index";
import { Routes, Route, Link } from "react-router-dom";
import News from './Pages/News/News';

function App() {
  return (
    <div className="App">
      {/* <Routes>
        <Route exact path="/" element={<Chat />}/>
        <Route exact path="/news" element={<News/>}/>
      </Routes> */}
     <Chat />
    </div>
  );
}

export default App;
