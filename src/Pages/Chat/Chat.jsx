import React from "react";
import Header from "../../Components/Header/header";
import Sidebar from "../../Components/SideBar/Sidebar";
import { Col, Row, Container } from "reactstrap";
import RightContent from "../RightContent";
import { Routes, Route, Link } from "react-router-dom";
import RightContentNews from "../RightContentNews";


function Chat() {
  return (
    <div>
      <div>
        <div className="header-Div">
          <Col lg="12" sm="12" className="header-col-height">
            <Header />
          </Col>
        </div>
      </div>
      <div>
        {/* <div className="sidebar-container">
          <Sidebar />
        </div> */}
        <div className="sidebar-container">
          <Col lg="3" sm="3" className="sidebar-content">
            <Sidebar />
          </Col>
          <Col lg="3" sm="3" className="right-content">
          <Routes>
        {/* <Switch> */}
        <Route exact path="/" element={<RightContent />}/>
        <Route exact path="/news" element={<RightContentNews/>}/>
        {/* </Switch> */}
      </Routes>
            {/* <RightContent /> */}
          </Col>
        </div>
      </div>
    </div>
  );
}

export default Chat;
