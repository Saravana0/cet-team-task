import React, { useEffect } from "react";
import {
  FormGroup,
  Label,
  Input,
  FormText,
  Button,
  ModalFooter,
  ModalHeader,
  ModalBody,
} from "reactstrap";
import FloatingLabel from "react-bootstrap/FloatingLabel";
import Form from "react-bootstrap/Form";
import Profile_2 from "../Assets/Profile2.jpg";
import Ferrari from "../Assets/Ferrari.jpg";
import { BsThreeDotsVertical } from "react-icons/bs";
import Popup from "../Components/Modal/modal";

function RightContentNews() {
  const descrData = [
    {
      id: 1,
      data: "Ferrari S.p.A. is an Italian luxury sports car manufacturer based in Maranello, Italy. Founded by Enzo Ferrari in 1939 from the Alfa Romeo racing division as Auto Avio Costruzioni, the company built its first car in 1940, and produced its first Ferrari-badged car in 1947",
    },
    {
      id: 2,
      data: "Ferrari S.p.A. is an Italian luxury sports car manufacturer based in Maranello, Italy. Founded by Enzo Ferrari in 1939 from the Alfa Romeo racing division as Auto Avio Costruzioni, the company built its first car in 1940, and produced its first Ferrari-badged car in 1947",
    },
    {
      id: 3,
      data: "Ferrari S.p.A. is an Italian luxury sports car manufacturer based in Maranello, Italy. Founded by Enzo Ferrari in 1939 from the Alfa Romeo racing division as Auto Avio Costruzioni, the company built its first car in 1940, and produced its first Ferrari-badged car in 1947",
    },
  ];

  const [modal, setModal] = React.useState(false);
  const [feedback, setFeedback] = React.useState();
  const [addArray, setAddArray] = React.useState(descrData);

  const setFeedbackContent = (event) => {
    // console.log(event.target.value)
    setFeedback(event.target.value);
  };

  const submittingContent = () => {
    // alert("hi");
    setAddArray((prev) => [...prev, { id: Date.now(), data: feedback }]);
    setModal(false);
  };

  console.log(addArray);

  const toggle = () => {
    setModal(!modal);
    // alert(modal)
  };

  // useEffect (() => {
  //     console.log(addArray.length);
  // },[modal])

  const popUpContent = () => {
    return (
      <div>
        {/* <FloatingLabel controlId="floatingTextarea2" label="Comments"> */}
        <div className="form-group">
          {/* <label htmlFor="exampleFormControlTextarea1">Basic textarea</label> */}
          <textarea
            className="form-control"
            id="exampleFormControlTextarea1"
            placeholder="Share your feedback"
            rows="8"
            onChange={(event) => setFeedbackContent(event)}
          />
        </div>
        {/* </FloatingLabel> */}
      </div>
    );
  };

  const topDescrData = [
    {
      id: 1,
      data: "Ferrari S.p.A. is an Italian luxury sports car manufacturer based in Maranello, Italy. Founded by Enzo Ferrari in 1939 from the Alfa Romeo racing division as Auto Avio Costruzioni, the company built its first car in 1940",
    },
    {
      id: 2,
      data: "Ferrari S.p.A. is an Italian luxury sports car manufacturer based in Maranello, Italy. Founded by Enzo Ferrari in 1939 from the Alfa Romeo racing division as Auto Avio Costruzioni, the company built its first car in 1940",
    },
    {
      id: 3,
      data: "Ferrari S.p.A. is an Italian luxury sports car manufacturer based in Maranello, Italy. Founded by Enzo Ferrari in 1939 from the Alfa Romeo racing division as Auto Avio Costruzioni, the company built its first car in 1940",
    },
  ];

  return (
    <div className="rightContent">
      <div className="new-feed-content">

        <div>
          <div className="new-feed-heder">
            <p className="margin_p news-header"> News Feed </p>
            <div className="new-sub-heder">
              <Form>
                <FormGroup className="margin-bottom-style ">
                  {/* <Label for="exampleSelect">Select</Label> */}
                  <Input
                    type="select"
                    name="select"
                    id="exampleSelect"
                    className="filter"
                    defaultValue=""
                  >
                    <option hidden value="">
                      Filter By
                    </option>
                    <option>2</option>
                    <option>3</option>
                  </Input>
                </FormGroup>
              </Form>
              <div>
                <button onClick={toggle}>
                  {" "}
                  <span className="plus"> + </span> Add Post{" "}
                </button>
              </div>
            </div>
          </div>
        </div>

<div className="news-feedOne">
{addArray.map((item) => (
          <div key={item.id} className="news-top-space">
            <div className="news-post">
              <div className="post-profile">
                <img src={Profile_2} alt="profile-pic" />
                <span className="profile-details">
                  <p className="margin_p profile_name"> John </p>
                  <p className="margin_p timing"> 8 min ago </p>
                </span>
                <div className="tag">
                  {" "}
                  <p className="tag-text"> feedback </p>{" "}
                </div>
              </div>
              <BsThreeDotsVertical />
            </div>
            <div className="image-content">
              <img src={Ferrari} alt="Car-image" />
            </div>

            <div className="Modal-description">{item.data}</div>
          </div>
        ))}
</div>
        
      </div>

      <div className="top-like-content ">
        <p className="margin_p top-header news-header">Top Like Post </p>

        <div className="sub-topPost-content">
          {topDescrData.map((item) => (
            <div className="top-liked-posts like-top-space" key={item.id}>
              <div className="top-like-news">
                <img
                  src={Profile_2}
                  alt="profile-pic"
                  className="top-like-profile-pic"
                />
                <span className="profile-details">
                  <p className="margin_p profile_name"> Name </p>
                  <p className="margin_p timing"> 8 min ago </p>
                </span>
              </div>
              <div className="carModel">
                <img src={Ferrari} alt="car" />
              </div>
              <div className="activity">
                <p className="margin_p"> 300 Likes </p>
                <p className="margin_p"> 30 Comments </p>
              </div>
              <div className="top-like-post-desc">{item.data}</div>
            </div>
          ))}
        </div>
      </div>

      {modal && (
        <Popup
          setModal={setModal}
          modal={modal}
          toggle={toggle}
          bodyContent={popUpContent()}
          submit={() => submittingContent()}
        />
      )}
    </div>
  );
}

export default RightContentNews;
