import React from 'react'

export default function RightContent() {
  return (
    <div className='rightContent d-flex justify-content-center align-items-center h-100 fs-2'> 
        Dashboard
    </div>
  )
}
