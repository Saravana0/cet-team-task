import React from "react";
import Header from "../../Components/Header/header";
import Sidebar from "../../Components/SideBar/Sidebar";
import { Col, Row, Container } from "reactstrap";
import RightContent from "../RightContent";
import RightContentNews from "../RightContentNews";

function News() {
  return (
    <div className="chat-container container">
      <Container>
        <Row className="header-Div">
          <Col lg="12" sm="12" className="header-col-height">
            <Header />
          </Col>
        </Row>
      </Container>
      <Container>
        {/* <div className="sidebar-container">
          <Sidebar />
        </div> */}
        <Row className="sidebar-container">
          <Col lg="3" sm="3" className="sidebar-content">
            <Sidebar />
          </Col>
          <Col lg="3" sm="3" className="right-content">
            <RightContentNews/>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default News